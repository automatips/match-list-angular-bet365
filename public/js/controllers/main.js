angular.module('matchController', [])

	.controller('mainController', ['$scope','$http','Matchs', function($scope, $http, Matchs) {
		$scope.formData = {};
		$scope.loading = true;

		Matchs.get()
			.success(function(data) {
				$scope.matchs = data;
				$scope.loading = false;
			});

	}]);